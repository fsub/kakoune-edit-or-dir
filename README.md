# kakoune-edit-or-dir

**Resurrected from [TeddyDD][TeddyDD]/[kakoune-edit-or-dir][kakoune-edit-or-dir]**

![tag](https://img.shields.io/gitlab/v/tag/fsub/kakoune-edit-or-dir)

Sometimes, when I use the `:edit` command in Kakoune it complains:`foo: is a
directory`. What if editing a directory would show an interactive file browser
instead?

## GIF time!

![Demo](https://gitlab.com/fsub/kakoune-edit-or-dir/uploads/b06c7c52436fb5fdc0cd4259fcdb5466/edit-or-dir-demo.gif)

## Installation

Add `edit-or-dir.kak` to your autoload directory
`${XDG_CONFIG_HOME}/kak/autoload` or any subfolder.
(`${XDG_CONFIG_HOME}` defaults to `${HOME}/.config`.)

For convenience, I recommend aliasing the `:edit-or-dir` command to `:e`.
I **do not** recommend aliasing or overriding the built-in `:edit` command
since it might brake other scripts.

```
unalias global e edit
alias global e edit-or-dir
```

## Usage

```
edit-or-dir [PATH]

PATH - relative or absolute path to file or directory.
       . means current directory
       .. means parent directory
       If PATH is a directory a file browser will show up in a temporary buffer called *dir*.
```

When in `*dir*` buffer you can use the following bindings:
- <kbd>Return</kbd>: Open selected files
- <kbd>Backspace</kbd>: Go to parent directory
- <kbd>Alt</kbd> <kbd>h</kbd>: Toggle hidden files
- <kbd>Escape</kbd>: Close the file browser

You can open multiple files after selecting them with <kbd>xJ</kbd>
or other means like <kbd>%</kbd><kbd>s</kbd>regex<kbd>Return</kbd>.

## Related plugins

- [occivink][occivink]/[kakoune-filetree][kakoune-filetree]
- [TeddyDD][TeddyDD]/[kakoune-lf][kakoune-lf]

## Changelog

- v0.7 2023-01-18:
  - __FIX__ adapt to breaking behavior change of `x` in Kakoune [v2022.10.31]
  - _CHANGE_ move `edit-or-dir.kak` from `rc` to root directory 
  - _CHANGE_ move `LICENSE.txt` to `UNLICENSE` and make minor adjustments
  - _CHANGE_ update `README.md`
  - _REMOVE_ `CONTRIBUTING` document, issue template, and `edit-or-dir.gif`
- **2023-01-18 RESURRECTED by [fsub][fsub]**
- **2020-01-02 DEPRECATED by [TeddyDD][TeddyDD]**
- v0.6 2019-06-16:
  - _ADD_ expand `~` in `:edit-or-dir` [#11]
  - _FIX_ don't show `..` in root PR: [#12]
  - _CHANGE_ refactoring PR: [#11] [#12]
- v0.5 2018-11-05:
  - Kakoune [v2018.10.27]
  - _ADD_ issue template
  - __CHANGE__ switch to directory layout with scripts in `rc` subdirectory (breaking: update source path in `kakrc`)
  - _FIX_ remove `--group-directories-first` flag from ls call (MacOS compatibility)
- v0.4 2018-10-13:
  - _ADD_ support to edit multiple files, PR [#4]
  - _ADD_ CONTRIBUTING document, PR [#4]
  - _ADD_ edit-or-dir-{forward,back} internal (hidden) commands, PR [#4]
  - _FIX_ Fix Typos, improve style and formatting of README, PR [#4]
  - _CHANGE_ rename edit-or-dir-display-dir (internal command, not breaking), PR [#4]
  - _CHANGE_ refactoring, PR [#4]
- v0.3 2018-09-28:
  - _ADD_ Hidden files toggle, PR [#2]
  - _ADD_ New bindings, PR [#2]
  - _ADD_ [EditorConfig] for developers
  - _FIX_ Remove new line from end of `*dir*` buffer
  - _FIX_ Apply changes proposed by [ShellCheck]
- v0.2 2018-09-27:
  - _FIX_ Replace `basename` with substitution, PR [#1]
- v0.1 2018-09-26:
  - Kakoune [v2018.09.04]
  - Initial release

[EditorConfig]: https://editorconfig.org
[ShellCheck]: https://shellcheck.net
[v2018.09.04]: https://github.com/mawww/kakoune/releases/tag/v2018.09.04
[v2018.10.27]: https://github.com/mawww/kakoune/releases/tag/v2018.10.27
[v2022.10.31]: https://github.com/mawww/kakoune/releases/tag/v2022.10.31

[#1]: https://github.com/TeddyDD/kakoune-edit-or-dir/pull/1
[#2]: https://github.com/TeddyDD/kakoune-edit-or-dir/pull/2
[#4]: https://github.com/TeddyDD/kakoune-edit-or-dir/pull/4
[#11]: https://github.com/TeddyDD/kakoune-edit-or-dir/pull/11
[#12]: https://github.com/TeddyDD/kakoune-edit-or-dir/pull/12

[occivink]: https://github.com/occivink
[kakoune-filetree]: https://github.com/occivink/kakoune-filetree

[TeddyDD]: https://github.com/TeddyDD
[kakoune-edit-or-dir]: https://github.com/TeddyDD/kakoune-edit-or-dir
[kakoune-lf]: https://github.com/TeddyDD/kakoune-lf

[fsub]: https://gitlab.com/fsub
